{{ config(
    materialized="table"
) }}


select

customer_code,
customer_name,
order_type,
price_list,
order_number,
order_date,
order_type,
item_code,
customer_txn_id,
invoice_number,
invoice_date,
currency_code,
exchange_rate,
unit_selling_price,
quantity_invoiced,
value_indian_currency,
oh_region

from XXK_SALES_REGISTER_FINAL_V