{{ config(
    materialized="table"
) }}

select
book_date,
invoice_no,
invoice_date,
days_taken,
po_number,
contractor_payable_amount,
contractor_invoice_no,
contractor_invoice_date,
contractor_invoice_amount,
contractor_payment,
cust_id,
cust_line_id,
taxable_value,
invoice_new,
contractor_payment_date,
vendor_name,
"NAME",
customer_name,
region,
karam_invoice_amount,
sales_person_name,
pricing_quantity,
order_cost,
order_cost_inr,
item_code
from APPS.XXK_CONTRACTOR_REGISTER_V