{{ config(
    materialized="table"
) }}


select
customer_id,
customer_name
days_past_due_inv,
due_date_inv,
invnum,
invdate,
purchase_order,
purchase_order_date,
amount,
pending,
amount_applied_inv,
amount_credited_inv,
amount_adjusted_inv,
gl_date,
cumm,
class_inv,
out_amt,
amount_due,
amount_undue,
"Less than 6 Months",
"GT 6 Months LT 1 Year",
"GT 1 Year LT 3 Year",
"GT 3 Year LT 5 Year",
"GT 5 Year",
"1-30 Days",
"31-90 Days",
"91-180 Days",
"181-365 Days",
"GT 365 Days",
territory,
payment_term,
currency_code,
exchange_rate,
total_amount_trx_currency

FROM APPS.XXK_DEBTOR_AGEING_REPORT_V 